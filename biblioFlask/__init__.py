import sqlite3 as sql
import os
import logging
import json

class BiblioFlask:

    def __init__(self):
        self.dirFiles = "files/"
        self.dbFile = 'data.db'
        self.champs = []
        self.items = []
        self.tables = {}

    def create_table(self, table):
        try:
            fields = []
            for field in table[1]:
                fields.append(field+' '+table[1][field])
            sql_cmd = "CREATE TABLE IF NOT EXISTS "+table[0]+" ("+','.join(fields)+")"
            conn = sql.connect(self.dbFile)
            conn.execute(sql_cmd)
            conn.close()
        except:
            print('error : ', table_name, 'table ')

    def create_db(self):
        try:
            for item in self.tables.items():
                self.create_table(item)
        except:
            print('error')

    def addFolder(self, id_item):
        try:
           os.makedirs(self.dirFiles+str(id_item)) 

        except OSError as e:
            print('error addfolder')
            raise(e)

    def addFile(self, id_item, file):
        try:
            self.addFolder(id_item)
            if file.filename == '':
                flash('No selected file')
                return redirect(request.url)
            else:
                file.save(self.dirFiles+str(id_item)+'/'+file.filename)
                return [0, file.filename]
            msg = ["0", "Record successfully added"]


        except OSError as e:
            print('error addfolder')
            raise(e)
    
    def add(self, table_name, request, folder=False):
        try:

            req = request.form.to_dict()
            if folder == True:
                file = request.files['file']
                req['file'] = file.filename
            else: 
                print('FALSEEE ------------------------')
            keys = []
            values = []
            for item in req.items(): keys.append(item[0]), values.append(item[1]),

            with sql.connect(self.dbFile) as con:
                cur = con.cursor()
                values_str = ', '.join([str("'"+elem+"'") for elem in values])

                sql_cmd = "INSERT INTO "+table_name+" ("+','.join(keys)+") VALUES ("+values_str+")" 
                cur.execute(sql_cmd)
                if folder == True:
                    self.addFile(cur.lastrowid, file) 
                con.commit()

        except Exception as e:
            print('error')
            raise(e)


    def remove(self, table_name, ID):
        try:
            with sql.connect(self.dbFile) as con:
                sql_action = 'DELETE FROM '+table_name+' WHERE id=?'
                cur = con.cursor()
                cur.execute(sql_action, (ID,))
            msg = ["0", "remove book "+str(ID)+" !!"]
        except:
            msg = ["1", "error"]
        return msg 
    
    def getAll(self, table_name, *args):
        con = sql.connect(self.dbFile)
        con.row_factory = sql.Row
        cur = con.cursor()
        cur.execute("SELECT "+','.join(args)+" FROM "+table_name)
        rows = cur.fetchall();
        # rowsJson = json.dumps(rows)
        return rows
    
    def get(self, table_name, ID, *args):
        con = sql.connect(self.dbFile)
        con.row_factory = sql.Row
        cur = con.cursor()
        cur.execute("SELECT "+','.join(args)+" FROM "+table_name+" WHERE id==?", (ID,))
        rows = cur.fetchall();
        return rows
    
    def modify(self, table_name, request, folder=False):
        try:
            keys = []
            values = []
            for item in request.items(): keys.append(item[0]), values.append(item[1]),

            with sql.connect(self.dbFile) as con:
                cur = con.cursor()
                values_str = ', '.join([str("'"+elem+"'") for elem in values])

                for item in request.items():
                    sql_cmd = "UPDATE "+table_name+" set "+item[0]+"='"+item[1]+"' where id="+request['id'] 
                    print(sql_cmd)
                    cur.execute(sql_cmd)
                con.commit()
        except:
            return 'error'






















        
        # return 'get info book '+str(idBook)+' !!'

# class Collections: 

#     def __init__(self):
#         self.dirFiles = ''
#         self.dbFile = ''
#         self.champs = []

#     def addFolder(self, idCollection):
#         try:
#             os.makedirs(self.dirFiles+str(idCollection))
#         except OSError as e:
#             if e.errno != errno.EEXIST:
#                 raise
    
#     def addFile(self, idCollection, files):
#         try:
#             pass
#         except:
#             return 'error'
        
#     def add(self, request):
#         msg = []
#         try:
#             collection = []
#             collection.append(request.form['title'])
#             collection.append(request.form['user'])
#             collection.append(request.form['date'])
#             collection.append(request.form['description'])
     
#             with sql.connect(self.dbFile) as con:
#                 cur = con.cursor()
#                 cur.execute(
#                         """INSERT INTO collections 
#                         ("""+','.join(self.champs)+""")
#                         VALUES (?,?,?,?)""",
#                         (collection))
#                 con.commit()
# @app.route('/create_collection_tab')
# def create_collection_tab():
#     try:
#         createCollectionTab()
#         return 'collection tab create;'
#     except:
#         return 'Error ! '
#             try:
#                 collection_id = cur.lastrowid
#                 self.addFolder(collection_id)
#                 file = request.files['file']
#                 if file.filename == '':
#                     flash('No selected file')
#                     return redirect(request.url)
#                 else:

#                     file.save(self.dirFiles+str(collection_id)+'/'+file.filename)
#                     return [0, file.filename]

#                 msg = ["0", "Record successfully added"]

#             except:
#                 msg = ["1", "Problème de upload du fichier"]
#                 logging.exception('')
#         except:
#             con.rollback()
#             msg = ["1", "error"]

#         return msg

#     def getAllCollections(self):
#         con = sql.connect("data.db")
#         con.row_factory = sql.Row
#         cur = con.cursor()
#         cur.execute("SELECT * FROM collections")
#         rows = cur.fetchall();
#         return rows
    
#     def getCollection(self, idCollection):
#         con = sql.connect("data.db")
#         con.row_factory = sql.Row
#         cur = con.cursor()
#         # cur.execute("SELECT * FROM books WHERE book_id=?", (idBook,))
#         cur.execute("SELECT * FROM books WHERE book_id=?", (idCollection,))
#         row = cur.fetchall();
#         return row
        
