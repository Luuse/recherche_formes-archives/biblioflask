#! /usr/bin/env python2

from setuptools import setup

setup(
    name='biblioflask',
    version='0.1',
    author='luuse',
    author_email='contact@luuse.io',
    description='systeme biblio',
    url='luuse.io',
    packages=['biblioFlask'],
    include_package_data=True,
    install_requires=[
        'flask'
    ],
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Operating System :: OS Independent',
        'License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)',
        'Intended Audience :: Developers',
        'Environment :: Web Environment',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Indexing/Search',
    ]
)
