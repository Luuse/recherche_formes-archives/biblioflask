import biblioFlask 
from flask import Flask, session, request, redirect, url_for, render_template
import os

# START BiblioFlask
BF = biblioFlask.BiblioFlask()
BF.dirFiles = "files/"
BF.dbFile = 'test.db'
BF.tables = {
                'documents' :{
                            'id': 'INTEGER PRIMARY KEY',
                            'title': 'TEXT',
                            'author': 'ARRAY',
                            'date': 'TEXT',
                            'type': 'TEXT',
                            'language': 'TEXT',
                            'file': 'TEXT',
                            'description': 'TEXT',
                            'user': 'TEXT',
                            'doctype': 'TEXT',
                            'collection': 'TEXT'
                            },
                'collections': {
                             'id' : 'INTEGER PRIMARY KEY',
                             'title' :  'TEXT',
                             'description' :  'TEXT'
                },
                'authors': {
                             'id' : 'INTEGER PRIMARY KEY',
                             'name' :  'TEXT',
                             'surname' :  'TEXT'
                }
            }

# START Flas
app = Flask(__name__, static_url_path='/static')

@app.route('/', methods=['POST', 'GET'])
@app.route('/collections/<id_collection>', methods=['POST', 'GET'])
def index(id_collection=False):

    if request.method == 'GET' and len(request.args) > 0:
        form=True
        action=request.args['action']
        table=request.args['table']
        id_item=request.args['id_item']
    else:
        form=True
        action=table=id_item=False

    if not os.path.exists(BF.dbFile): BF.create_db()

    items = {}
    for table in BF.tables:
        items[table] = BF.getAll(table, '*')
    return render_template('index.html',
            tables=BF.tables,
            items=items,
            form=form,
            action=action,
            table=table,
            id_item=str(id_item),
            collection_selected=id_collection
            )

@app.route('/add-doc', methods=['GET', 'POST'])
def addDoc():
    if request.method == 'POST':
        BF.add('documents', request, True)
    return redirect(url_for('index'))

@app.route('/add-collection', methods=['GET', 'POST'])
def addCol():
    if request.method == 'POST':
        BF.add('collections', request, False)
    return redirect(url_for('index'))

@app.route('/remove/<table>/<id_item>')
def remove(table,id_item):
    try:
        BF.remove(table, id_item)
        return redirect(url_for('index'))
    except:
        print('error')

@app.route('/form_modify/<table>/<id_item>')
def form_modify(table,id_item):
    try:
        return redirect(url_for('index', action='modify', table=table, id_item=id_item))
    except:
        print('error')
    
@app.route('/modify', methods=['POST'])
def modify():
    if request.method == 'POST':
        BF.modify('documents', request.form.to_dict())
    return redirect(url_for('index'))

app.run(debug=True)
